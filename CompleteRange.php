<?php
	//Se crea la clase CompleteRange
	class CompleteRange
	{
		//El metodo build
		function build($list)
		{
			//Se cuenta cuantos elementos tiene el array ingresado
			$size = count($list);
			//Si el array es igual a 0 que devuelva vacion
			if ($size == 0)
				return [];
			//Sino que guarde en la variable start el primer elemento del array
			$start = $list[0];
			//Guarda en la variable end el ultimo elemento del array
			$end = $list[$size-1];
			//Con la funcion range completa los elementos que faltan
			$array = range($start, $end);
			//Se une los elementos del array separado por comas
			return "[".implode(",", $array)."]";
		}
		
	}
	$result = CompleteRange::build([1,2,4,5]);
	print_r($result);
	echo '<br>';	
	$result = CompleteRange::build([2,4,9]);
	print_r($result);
	echo '<br>';
	$result = CompleteRange::build([55,58,60]);
	print_r($result);
	echo '<br>';
<?php
	// estado 0: Esperamos un ( -> UNO
	// estado 1: Esperamos un ) -> CERO y considerar "()"
	//Se crea la clase ClearPar
	class ClearPar
	{
		//Se crea la variable estado
		private $estado;
		//Se crea el metodo build
		static function build($string)
		{
			//Se inicializa en 0
			$estado = 0;
			//Y el resultado vacio
			$resultado = '';
			//Se recorre el parametro ingresado
			for ($i=0; $i<strlen($string); ++$i) {
				//Si el estado es igual a 0 y la letra el "(" el estado cambia a 1
				if ($estado == 0 && $string[$i]=='(') {
					$estado = 1;
				//Sino si estado es igual a 1 y la letra es ")" entonces el resultado sera "()"
				} else if ($estado == 1 && $string[$i]==')') {
					$resultado .= '()';
					$estado = 0;
				}
			}
			return $resultado;
		}
	}
	echo ClearPar::build("()())()"); // salida : "()()()"
	echo '<br>';
	echo ClearPar::build("()(()"); // salida : "()()"
	echo '<br>';
	echo ClearPar::build(")("); // salida : ""
	echo '<br>';
	echo ClearPar::build("((()"); // salida : "()"
<?php

	//Se crea la clase ChangeString
	class ChangeString
	{
		//Se declara una variable para el metodo constructor
		private $alphabet;
		function __construct($alphabet)
		{
			$this->alphabet = $alphabet;
		}

		//Se crea el metodo build el cual recibira el parámetro String
		function build($string)
		{
			//Se realiza un bucle para recorrer el parámetro ingresado, caracter por caracter
			for ($i=0; $i<strlen($string); ++$i) {

				$string[$i] = $this->buildCharacter($string[$i]);
			}
			return $string;
		}

		//Se crea un metodo para que realice la comparación de cada letra o numero dentro del string
		function buildCharacter($c)
		{
			//Se compara si el string(caracter) ingresado es mayuscula se guarda en la variable $upper
			$upper = ($c == strtoupper($c));
			//Se busca el string en alphabet- Array_search me devolvera el key del array en alphabet
			$pos = array_search(strtolower($c), $this->alphabet);
			//Si el caracter buscado no lo encuentra en alphabeth			
			if ($pos === false)
			//Me retornara la misma letra
				return $c;			
			($pos == count($this->alphabet)-1)?$pos=0:$pos+=1;
			//Si es mayuscula entonces lo va a poner en mayuscula la sgte letra
			if ($upper)
				return strtoupper($this->alphabet[$pos]);
			return $this->alphabet[$pos];
		}
	}

	$changeString = new ChangeString([ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'ñ', 'o', 'p', 'q', 'r',
								's', 't', 'u', 'v', 'w', 'x', 'y', 'z' ]);
	
	echo $changeString->build("123 abcd*3");
	echo '<br>';
	echo $changeString->build("**Casa 52");
	echo '<br>';
	echo $changeString->build("**Casa 52Z");